CREATE TABLE IF NOT EXISTS branch (
    branch_id INT PRIMARY KEY NOT NULL,
    branch_addr VARCHAR(200),
    ISBN VARCHAR(100),
    title VARCHAR(100),
    author VARCHAR(50),
    publisher VARCHAR(100),
    num_copies INT DEFAULT 0
);

