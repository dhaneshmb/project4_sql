CREATE TABLE IF NOT EXISTS manager (
    manager_id INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
    manager_name VARCHAR(50),
    manager_location VARCHAR(200),
);

CREATE TABLE IF NOT EXISTS contract_table (
    contract_id INT PRIMARY KEY AUTO_INCREMENT NOT NULL
    estimated_cost INT DEFAULT 0,
    completion_date DATE
); 

CREATE TABLE IF NOT EXISTS staff (
    staff_id INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
    staff_name VARCHAR(50),
    staff_location VARCHAR(200)
);

CREATE TABLE IF NOT EXISTS client (
    client_id INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
    client_name VARCHAR(50),
    client_location VARCHAR(200),
    manager_id INT,
    contract_id INT,
    staff_id INT,
    FOREIGN KEY (manager_id) REFERENCES manager(manager_id),
    FOREIGN KEY (contract_id) REFERENCES contract_table(contract_id),
    FOREIGN KEY (staff_id) REFERENCES staff(staff_id)
);