CREATE TABLE IF NOT EXISTS patient (
    patient_id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    patient_name VARCHAR(50),
    patientDOB DATE,
    patient_address VARCHAR(200)   
);

CREATE TABLE IF NOT EXISTS prescription (
    prescription_id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    drug VARCHAR(100),
    prescription_date DATE,
    dosage VARCHAR (100),
);

CREATE TABLE IF NOT EXISTS doctor (
    doctor_id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    doctor_name VARCHAR(50),
    secretary VARCHAR(50),
    patient_id INT,
    prescription_id INT,
    FOREIGN KEY(patient_id) REFERENCES patient(patient_id),
    FOREIGN KEY(prescription_id) REFERENCES prescription(prescription_id)
);