CREATE TABLE IF NOT EXISTS prescription (
    prescription_id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    drug VARCHAR(100),
    prescription_date DATE,
    dosage VARCHAR(150),
    doctor VARCHAR(50),
    secretary VARCHAR(50)
);

CREATE TABLE IF NOT EXISTS patient (
    patient_id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    patient_name VARCHAR(50),
    DOB DATE,
    patient_address VARCHAR(200),
    prescription_id INT,
    FOREIGN KEY (prescription_id) REFERENCES prescription(prescription_id)
);
